import React from 'react';
import { StyleSheet, Text, TextInput,
     View, Button, Platform, Image } from 'react-native';

export default class FirstScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { myName: "" };
  }
  componentWillMount() {
    console.log("componentWillMount")
  }
  componentDidMount() {
    console.log("componentDidMount")  
  }

  render() {
    console.log("render")
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.props.name}</Text>
        <Text style={styles.text}>{this.state.myName}</Text>
         <Button style={styles.button}
          onPress={() => this.props.navigation.goBack()}
          color="#ff0000"
          title="Meu Primeiro Button"/>

          <TextInput style={styles.input}
            onChangeText={(text) => {
              this.setState({
                myName : text
              })
            }}>
          
          </TextInput>
          <Image
            style={{height: 50, width: 50}}
            source={{uri: "https://facebook.github.io/react-native/docs/assets/favicon.png"}}/>
      </View>
    );
  }

  onClick() {
    console.log("onClick")
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: "row",
    alignItems: "center",
    justifyContent: 'space-between',
  },
  text: { 
    backgroundColor: Platform.OS === 'android' ? '#ff0000' : '#00ff00',
  },
  input: {
    height: 40,
    width: 100,
    borderColor: 'gray',
    backgroundColor: '#ddd'
  },
  button: {
    borderColor: 'red',
    borderWidth: 5,
    borderRadius: 15,
    backgroundColor: '#00aeef'
  }
});

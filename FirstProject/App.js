import React from 'react';
import { StyleSheet,
     View, Text , Button, TouchableOpacity} from 'react-native';
import FirstScreen from './FirstScreen';
import DummyView from './DummyView';
import { createStackNavigator } from "react-navigation"
export default class App extends React.Component {

  render() {
    return (
     <RootStack/>
    );
  }

}

const RootStack = createStackNavigator({
  Home: DummyView,
  Details: FirstScreen
}, {
  initialRouteName: "Home"
});

const styles = StyleSheet.create({
  container: {
    flex: 100,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 50
  },
  son30: { 
    flex: 30,
    flexDirection: "column"
  }, 
  son70: {
    flex: 70,
    flexDirection: "column"
  }
});

import React, {Component} from 'react';
import {View, Text, AsyncStorage} from 'react-native';

export default class TestSaving extends Component {

    constructor(props) {
        super(props)
        this.state = {
            savedValue: ""
        };
    }

    componentWillMount() {
        this.storeData = async () => {
            try {
              await AsyncStorage.setItem('teste2', 'I like to save it.22222');
            } catch (error) {
                console.log(error)
            }
          }
        this.storeData();
    }

    async componentDidMount() {
        this.retrieveData = async () => {
            try {
              const value = await AsyncStorage.getItem('teste2');
              if (value !== null) {
                this.setState({
                    savedValue: value
                })
              }
              console.log(value);
             } catch (error) {
                console.log(error)
             }
          }
        this.retrieveData();
    }


    render() {
        console.log("render")
       return (<View style={{flex:1}}>
            <Text>{this.state.savedValue}</Text>
        </View>)
    }
}
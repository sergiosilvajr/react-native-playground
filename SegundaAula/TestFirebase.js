import firebase from '@firebase/app'
import '@firebase/auth'
import '@firebase/database'

import React, {Component} from 'react';
import {View} from 'react-native';

export default class TestFirebase extends Component {

    async componentWillMount() {
        var config = {
            apiKey: "AIzaSyAvjyXDp-9FK0OT8jckTQ9cXLjqaEO-zCg",
            authDomain: "fir-sample-22071.firebaseapp.com",
            databaseURL: "https://fir-sample-22071.firebaseio.com",
            projectId: "fir-sample-22071",
            storageBucket: "fir-sample-22071.appspot.com",
            messagingSenderId: "1058127461484"
          };

          if (!firebase.apps.length) {
            firebase.initializeApp(config);
          }

          await this.signup("zzz@zzzz.com", "123456")
          await this.insertData()
    }

    async insertData() {
        try{
            const currentUser = await firebase.auth().currentUser
            const id  = await currentUser.uid
            console.log(id)
            const result = firebase.database()
                .ref(`users/${id}`).set({
                    username: 'nome',
                    email: 'whatever',
                    profile_picture : 'url'
                  });
        } catch (error) {
            console.log(error.toString())
        }
    }
    
    async signup(email, pass) {
        try {
            const credentials = await firebase.auth()
                .signInWithEmailAndPassword(email, pass)

            console.log(credentials.user.email)
            return credentials
        } catch (error) {
            console.log((error.toString()))
        }
    
    }
    render() {
        return (
            <View style={{flex: 1}}>
            </View>
        )
    }
}
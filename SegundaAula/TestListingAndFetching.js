import React, {Component} from 'react';
import {View, Text, AsyncStorage, FlatList
,TouchableHighlight, Image} from 'react-native';

export default class TestListingAndFetching extends Component {

    constructor(props) {
        super(props)
        this.state = {
            savedValue: "",
            movies: []
        };
    }

    async componentDidMount() {
        const movies = await this.fetchList()
        const moviesMap = movies.map((movie, index) => {
            return {title: movie.title,
                    key : `${index}`,
                    path: `https://image.tmdb.org/t/p/w500${movie.poster_path}`}
        })
        this.setState({
            movies: moviesMap
        })
    }

    async fetchList() {
        try {
            const url = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=4895a1e4cec2aeb113fc7178193f3920"
            const result = await fetch(url);
            const list = await result.json();

            return list.results;
        } catch (error) {
            console.error(error)
        }
    }

    render() {
        console.log("render")
       return (<View style={{flex:1}}>
            <Text>{this.state.savedValue}</Text>
            <FlatList
                    data={this.state.movies}
                    renderItem={({item}) => 
                        <TouchableHighlight
                            style={{flex:1}}
                            onPress={() => 
                            console.log(item)}>
                            <View>
                                <Text>{item.title}</Text>
                                <Image style={{width: 150, height: 100}} source={{uri : item.path}}/>
                            </View>
                        </TouchableHighlight>}>
               </FlatList>
        </View>)
    }
}
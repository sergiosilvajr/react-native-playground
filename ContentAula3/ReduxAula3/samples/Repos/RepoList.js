import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { listRepos, listReposCoutinho } from './actions/index';

class RepoList extends Component {
  componentDidMount() {
    this.props.listRepos(0, 'sergiosilvajr')
    this.props.listRepos(1, 'koitim')
    this.props.listRepos(2, 'arthursb')
  }
  renderItem = ({ item }) => (
    <View style={styles.item}>
      <Text>{item.name}</Text>
    </View>
  );
  render() {
    const { repos } = this.props;
    const foo = () => {
        let list = []
        for(let i = 0; i < 3; i ++){
            list.push( <FlatList
            style={{width: 250, height: 100}}
            data={repos}
            renderItem={this.renderItem}
        />)
        }
        return list
    }
    return (
        <View>
        {this.props.loading ? <ActivityIndicator/> : undefined}
        <View styles={styles.container}>
            {foo()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  item: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  }
});

const mapStateToProps = state => {
  let storedRepositories = state.repo
  return {
    repos: storedRepositories,
    position: state.position,
    loading: state.loading
  };
};

const mapDispatchToProps = {
  listRepos
};

export default connect(mapStateToProps, mapDispatchToProps)(RepoList);
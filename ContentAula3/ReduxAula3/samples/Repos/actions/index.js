export const GET_REPOS = 'my-awesome-app/repos/LOAD';
export const GET_REPOS_SUCCESS = 'my-awesome-app/repos/LOAD_SUCCESS';
export const GET_REPOS_FAIL = 'my-awesome-app/repos/LOAD_FAIL';


export function listRepos(position, user) {
    const action = {
      type: GET_REPOS,
      payload: {
        request: {
          url: `/users/${user}/repos`
        }
      }
    };
    return action
  }


export function listReposCoutinho() {
    const action = {
      type: GET_REPOS,
      payload: {
        request: {
          url: `/users/koitim/repos`
        }
      }
    };
    return action
  }
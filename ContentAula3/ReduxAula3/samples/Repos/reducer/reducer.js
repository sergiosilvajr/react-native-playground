
import {GET_REPOS, GET_REPOS_SUCCESS, GET_REPOS_FAIL} from '../actions/index.js'

const INITIAL_STATE = {
    repos: undefined,
    loading: false,
    error: undefined
}

export default function reducer(state = INITIAL_STATE, action) {
 
  switch (action.type) {
    case GET_REPOS:
      return { ...state, loading: true };
    case GET_REPOS_SUCCESS: { 
      const list = []
      list.push(action.payload.data)
     
      const newState =  { ...state, loading: false, repos: list, position: action.payload.position };
      console.log("newState")

      console.log(newState.repos)
      return newState
    }
    case GET_REPOS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}
